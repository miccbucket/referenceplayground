/**
 * 
 */
package it.unifi.micc.java_base;

/**
 * @author seide
 *
 */
public class Square {
	public float getX() {
		return x;
	}
	public void setX(int x) {
		this.x = x;
	}
	public float getY() {
		return y;
	}
	public void setY(int y) {
		this.y = y;
	}
	public float getSide() {
		return side;
	}
	public void setSide(float side) {
		this.side = side;
	}
	@Override
	protected Object clone() throws CloneNotSupportedException {
		Square clonedSquare = new Square(this.x,this.y,this.side);
		return clonedSquare;
	}
	private float x;
	private float y;
	private float side;
	@Override
	public String toString() {
		return "Square: [" +x+","+y+"," + side +"]";
	}
	@Override
	public boolean equals(Object obj) {
		if (obj==null){
			return false;
		} else {
			Square toCompare=(Square)obj;
			return (toCompare.x==this.x && toCompare.y==this.y && toCompare.side==this.side );
		}
	}
	public double getArea(){
		return side*side;
	}
	public double getPerimeter(){
		return side*4;
	}
	public Square(float side, float x, float y){
		this.x=x;
		this.y=y;
		this.side=side;
	}
	public Square(Square s) {
		this.x=s.x;
		this.y=s.y;
		this.side=s.side;
	}
	
}
