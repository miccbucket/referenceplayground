package it.unifi.micc.java_base;

public class ReferencePlayground {
	
	
	
	

	public static void main(String[] args) {
		
		
		
		
		Square s = new Square(10,0,0);
		Square s1=s;
		Square s2=new Square(2,2,2);
		
		System.out.println("Area s: " + s.getArea());
		System.out.println("Area s1: " +s1.getArea());
		System.out.println("Area s2: " +s2.getArea());
		
		s1.setSide(4); //Modifying the side of the Square object pointed by s and s1.
		
		System.out.println("====");
		
		System.out.println("Area s: " + s.getArea());
		System.out.println("Area s1: " +s1.getArea());
		System.out.println("Area s2: " +s2.getArea());
		System.out.println("====");
		
		//Checking equality
		System.out.println("s==s1 " +(s==s1));
		
		System.out.println("s==s2 " +(s==s2));
		
		System.out.println("====");
		
		Square s3 = new Square(s);
		
		System.out.println("s3: "+ s3);
		System.out.println("s: "+ s);
		System.out.println("s==s3 " +(s==s3));
		
		System.out.println("s.equals(s3) " +(s.equals(s3)));

		System.out.println("s==s2 " +(s==s2));
		System.out.println("s.equals(s2) " +(s.equals(s2)));
		
		
		
		
	}

}
